【盛筑OA】是智能数字化的协同办公自动化管理系统，为企事业单位提供优质而高效地处理办公事务和业务信息，实现对信息资源的高效利用，贯通业务，优化流程，最大限度地提高工作效率和质量、改善工作环境。盛筑OA系统集成个人办公、行政、人事、考勤、公文、业务、销售、客户、资产、流程、项目、财务等管理，为企业管理提供一站式解决“人、财、物、事”等数字化难题。

JAVA版待发布